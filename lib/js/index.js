// Import everything here, including the generated TypeScript file

// Import the Zepto modules we need
import 'zepto/src/zepto.js';
import 'zepto/src/event.js';
import 'zepto/src/form.js';
import 'zepto/src/ie.js';
import 'zepto/src/fx.js';
import 'zepto/src/fx_methods.js';
import 'zepto/src/selector.js';

// Import styles
import '../sass/style.sass';

// Testing TypeScript
import './typescript/index.js';
