# Pipette
The style and JavaScript library for DataJoe's Beaker.

Beaker uses Pipette to build components for DataJoe's applications.

## Requirements
Please make sure TypeScript is installed globally. You should be using it anyway if you write JavaScript.
`npm i -g typescript`

## Using
Beaker uses Pipette by default, so you should only be developing this project via git.

In order to import this project into another, you have to include multiple files:
* For JS, `import 'pipette'`
* For CSS, `@import 'pipette/dist/pipette.css'`

## Building
The JavaScript in this project is built using Rollup.
* `pipette.esm.js` is the ES module build for use in Webpack.

The CSS in this project is built with SASS:
* `pipette.css` contains all the CSS for the project.

To build the files yourself:  
`npm run build` will build out the files once.  
`npm run watch` will watch the files for any changes and generate new ones on save.

## Linting
* The JS in this project uses ESLint. Please be sure your IDE is hooked up properly.
* Please define all globals in a comment at the top of your file.

For example:
```
/* global window */

// Rest of code here.
```

## Why TypeScript?
When JavaScript projects get bigger, the lack of type hinting can lead to serious
issues that are incredibly difficult to track down. Using TypeScript allows the
developer more control over his code, which lets the project scale much easier.

Please consider investing some time to learning TypeScript if you haven't already and utilize the features when writing custom code.

The build system will also yell at you if you're breaking something or doing something that TypeScript doesn't like. This is an additional code quality step to keep this package to standard.

#### How does this work with the build system?
If you can, set up your IDE with TypeScript integration.
The current `tsconfig.json` allows for auto-compilation on save.

When you build the project using `npm run build`, it first transpiles `lib/ts/*.ts` to `lib/js/typescript/*.js` which gets imported to `lib/js/index.js` along with the other libraries we use.

Rollup then pulls it all together and outputs it to `dist/pipette.esm.js`.

## Why SASS?
SASS (.sass not .scss) uses indentation instead of brackets and semicolons to define styles. This
requires the developer to be more strict with style and will keep the health of this
project's CSS high over the years.
