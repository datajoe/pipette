const path = require('path');
const babel = require('rollup-plugin-babel');
const scss = require('rollup-plugin-scss');
const resolve = require('rollup-plugin-node-resolve');

const base = path.resolve(__dirname, './');
const lib = path.resolve(base, 'lib/js');
const dist = path.resolve(base, 'dist');

const name = 'pipette';

module.exports = {
    entry: path.resolve(lib, 'index.js'),
    moduleName: 'Pipette',
    plugins: [
        scss({
            output: path.resolve(dist, `${name}.css`),
            indentedSyntax: true,
            outputStyle: 'compressed',
        }),
        babel({
            exclude: 'node_modules/**',
        }),
        resolve(),
    ],
    targets: [
        {
            format: 'es',
            dest: path.resolve(dist, `${name}.esm.js`),
            sourceMap: true,
        },
    ],
};
